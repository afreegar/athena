#include "../DumpDecisions.h"
#include "../RoRSeqFilter.h"
#include "../TriggerSummaryAlg.h"
#include "DecisionHandling/ComboHypo.h"
#include "../InputMakerForRoI.h"
#include "../DeltaRRoIComboHypoTool.h"
#include "DecisionHandling/ComboHypoToolBase.h"

#include "../ViewCreatorInitialROITool.h"
#include "../ViewCreatorPreviousROITool.h"
#include "../ViewCreatorNamedROITool.h"
#include "../ViewCreatorFSROITool.h"
#include "../ViewCreatorFetchFromViewROITool.h"
#include "../ViewCreatorCentredOnIParticleROITool.h"
#include "../ViewCreatorCentredOnClusterROITool.h"
#include "../ViewCreatorCentredOnJetWithPVConstraintROITool.h"

DECLARE_COMPONENT( DumpDecisions )
DECLARE_COMPONENT( RoRSeqFilter )
DECLARE_COMPONENT( TriggerSummaryAlg )
DECLARE_COMPONENT( ComboHypo )
DECLARE_COMPONENT( InputMakerForRoI )
DECLARE_COMPONENT( ComboHypoToolBase )
DECLARE_COMPONENT( DeltaRRoIComboHypoTool )

DECLARE_COMPONENT( ViewCreatorInitialROITool )
DECLARE_COMPONENT( ViewCreatorPreviousROITool )
DECLARE_COMPONENT( ViewCreatorNamedROITool )
DECLARE_COMPONENT( ViewCreatorFSROITool )
DECLARE_COMPONENT( ViewCreatorFetchFromViewROITool )
DECLARE_COMPONENT( ViewCreatorCentredOnIParticleROITool )
DECLARE_COMPONENT( ViewCreatorCentredOnClusterROITool )
DECLARE_COMPONENT( ViewCreatorCentredOnJetWithPVConstraintROITool )



